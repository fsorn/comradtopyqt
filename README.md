# Notes

## Steps I followed for the transformation

1. Design ui file in comrad
2. pyuic5 app.ui -o app.py
3. Write main function with QApplication instantiation
4. Replace Comrad widgets in setupUI() with Qt equivalents if possible + remove setting channels
5. Write own data acquisition for the pure PyQt widgets.

For pure PyQt widgets I did not experience any problems, they would not have to be
changed in any way. Signals and Slot connections between them worked fine as well.

For comrad widgets, it is easy if there is an obvious replacement (QLabel, QLineEdit).
For f.e. PyDmByteIndicator you have no replacement, so you are stuck with the comrad
dependency there, but you can theoretically switch away from the channels to setting
values directly.

## Some "Problems" / things to consider

1. It would be nice to have a "Export as PyQt" option
   This could export your UI file as a Python module + main function
   initializing the QApplication. From there the user would already have
   a running PyQt application and could exchange widgets piece per piece.

   One thing that is missing is the PYDM_DATA_PLUGIN env variable, when
   you are converting the comrad application to pure pyqt without using
   the comrad launcher. The error which comes up is not helpful in pointing
   out the error.
2. Users have to know about PyJapc, there is no hint anywhere, how comrad
   acquires its data. The code only hints to some channel or address as a
   property of the widget. When switching to pure PyQt, they might not know,
   where to start.

   Maybe a class like the one in qtpyjapc.py could be also be part of an
   "Export as PyQt" Action.
3. UI Elements are created in the GUI Thread, while PyJapc callbacks are executed
   in another thread spawned by PyJapc. The user has to know that he should
   only touch gui widgets in the gui thread, nothing is preventing him from doing
   otherwise (Changing a label's text in the PyJapc callback thread worked).

   Maybe some class like the one in qtpyjapc.py could be a solution for that.
   This class could allow users to subscribe to some parameter and make sure that
   the passed callback is executed in the GUI thread.