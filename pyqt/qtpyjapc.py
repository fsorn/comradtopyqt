from typing import Callable, Dict, Any

from qtpy import QtCore
import pyjapc


class QtPyJapcConnection(QtCore.QObject):

    """
    In Qt Applications, GUI components should not be manipulated from any
    other Thread than the gui thread. PyJapc on the other hand calls its
    callbacks in a separate thread, so directly manipulating gui components
    from the callback is not a nice solution.
    For this we use a signal in this class, which emits the new value as well
    as the gui manipulating callback, which allows us to easily execute the
    gui manipulation in the main thread and not in PyJapc's callback thread.
    The slot, this signal is connected to, will take the callback reference
    and call it with the new value in the right Thread.
    """

    japc = pyjapc.PyJapc()
    japc.setSelector("")

    sig_new_value = QtCore.Signal(object)

    def __init__(self):
        super().__init__()
        self.sig_new_value.connect(
            QtPyJapcConnection._callback_in_gui_thread
        )

    def subscribe(self, param: str, value_transform: Callable, callback: Callable):
        """
        Subscribe to a given parameter.

        Args:
            param: parameter that you want to subscribe to
            value_transform: function with transforms a received value to a
                             another data type
            callback: Callback for the GUI manipulation
        """
        self.japc.subscribeParam(
            parameterName=param,
            getHeader=True,
            onValueReceived=lambda field_name, value, header: self._on_val_rec(
                field_name, value, header, value_transform, callback
            ),
        )

    def start(self):
        self.japc.startSubscriptions()

    def _on_val_rec(
            self,
            field_name: str,
            value: Any,
            header: Dict,
            value_transform: Callable,
            callback: Callable
    ):
        data = value_transform(field_name, value, header)
        value = (callback, data)
        self.sig_new_value.emit(value)

    @staticmethod
    def _callback_in_gui_thread(val: object):
        callback = val[0]
        data = val[1]
        callback(data)
